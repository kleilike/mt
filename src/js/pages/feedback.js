$(function() {
   var $feedback = $('.feedback');

   if ($feedback.length == 0) {
       return;
   }

   console.log(1);

    var inst = $('[data-remodal-id="modal-feedback-accepted"]').remodal();


    $('.feedback form').validate({
        rules: {
            company: "required",
            name: "required",
            phone: "required",
            email: {
                required: true,
                email: true
            },
            type: "required",
            // email: {
            //     required: true,
            //     email: true
            // }
        },
        messages: {
            company: "",
            name: "",
            phone: "",
            email: "",
            type: "",
            quest: "",
            // email: {
            //     required: "We need your email address to contact you",
            //     email: "Your email address must be in the format of name@domain.com"
            // }
        },
        submitHandler: function() {
            inst.open();
        }
    });
});