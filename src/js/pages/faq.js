$(function() {
    var $faq = $('.faq-item');

    if ($faq.length == 0) {
        return;
    }

    $(document).on('click', '.faq-item__header', function(e) {
        e.preventDefault();
        var $faq = $(this).parents('.faq-item');
        var $faqBody = $faq.find('.faq-item__body');

        if ($faq.hasClass('faq-item--open')) {
            $faqBody.stop().slideUp();
            $faq.removeClass('faq-item--open');

        } else {
            $faqBody.stop().slideDown();
            $faq.addClass('faq-item--open');
        }
    });

});