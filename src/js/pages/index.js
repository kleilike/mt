$(function () {
    var $clients = $('.clients__list');

    if ($clients.length == 0) {
        return
    }

    $clients.slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
});

$(function() {
    var video = document.querySelector('video');
    
    if (!video) {
        return;
    }

    $(window).scroll(function() {
        video.play();
        // alert(1);
    })
})

$(function() {
    var $achievementsNumber = $('.achievements-number');
    var $item;
    var value = [];
    var flag = false;

    if ($achievementsNumber.length == 0) {
        return;
    }

    $('.achievement-number__value').each(function(index, elem) {
        var num = parseInt( $(elem).html() );
        value.push(num);
        $(elem).html('0');
    });

    $('.achievements-number').waypoint(function(direction) {
        if (!flag) {
            flag = true;
            
            if (direction === 'down') {
                $item = $achievementsNumber.find('.achievement-number__value');
                  $item.each(function(index, elem) {
                      $(elem).delay(500).animateNumber({
                          number: value[index],
                          easing: 'easeInQuad',
                      });
                  })
            }
        }
    }, {
        offset: '75%'
    });
})

$(function() {
    var form = $('.b-main-form');

    if (form.length == 0) { 
        return;
    }

    $(document).on('focus', '.b-main-form input', function() {
        $('.b-main-form .g-recaptcha').show();
    })
});

$(function() {
    var $bMainBannerSlider = $('.b-main-banner-slider');

    if ($bMainBannerSlider.length) {
        $bMainBannerSlider.find('.b-main-banner-slider__list').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            dots: true
        });
    }

});