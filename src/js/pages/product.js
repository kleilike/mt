$(function () {
    var $productView = $('.product-view');
    var $sliderFor = $productView.find('.product-view-for');
    var $sliderNav = $productView.find('.product-view-nav');

    if ($productView.length) {
        $sliderFor.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.product-view-nav'
        });

        $sliderNav.slick({
            centerPadding: '20px',
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.product-view-for',
            dots: true,
            centerMode: true,
            focusOnSelect: true
        });
    }


    var $tooltip = $('.tooltip');

    if ($tooltip.length > 0) {
        $tooltip.tooltipster({
            maxWidth: 360,
            side: 'right'
        });
    }
});

$(function () {
    $(document).on('change', '.product-options input', function (e) {
        var sum = $(this).parents('.checkbox').attr('data-price');
        var sumOld = $('.product-result .price span').html();
        sum = parseInt(sum);
        sumOld = parseInt(sumOld.replace(' ', ''));

        if (!$(this).prop('checked')) {
            sum = -sum;
        }

        var newSum = sumOld + sum;


        $('.product-result .price span ').html(number_format(newSum, 0, '.', ' '));
        $('.label-price').html(number_format(newSum, 0, '.', ' ') + ' руб');

    })
});

function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k)
                    .toFixed(prec);
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
            .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
            .join('0');
    }
    return s.join(dec);
}


$(function() {
    var $form = $('.modal-product form');

    if ($form.length == 0) {
        return;
    }

    $('.modal-product form').validate({
        rules: {
            name: "required",
            phone: "required",
        },
        messages: {
            name: "",
            phone: "",
        }
    });
});