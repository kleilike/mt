// ВЫРОВНИТЬ ВЫСОТУ
// Для инициализации добавить data-height="name"
// name - общее имя для блоков
$(function() {
    var $dataHeight = $('[data-height]');
    var arr = [];

    if ($dataHeight.length == 0) {
        return;
    }

    $dataHeight.each(function(index, elem) {
        var name  = $(elem).attr('data-height');

        if (arr.indexOf(name) == -1) {
            arr.push(name);
        }

        changeHeight(name);
        $(window).on('resize', changeHeight(name));
    });


    function changeHeight(name) {
        var $tag = $('.' + name);
        var maxHeight = 0;

        $tag.css('height', 'initial');

        $tag.each(function(index, elem) {
            var height = $(elem).height();

            if (maxHeight < height) {
                maxHeight = height;
            }
        });

        $tag.css('height', maxHeight);
    }
})