// ПОДМЕНЮ
// работает мобильном

$(function() {
    var haederNav = $('.header-nav');

    if ($('.header-nav').length == 0) {
        return;
    } 

    $(document).on('click', '.header-nav__drop-link', function() {
        $(this)
            .parent('.header-nav__drop')
            .toggleClass('open');
    })
});


// МЕНЮ

$(function() {
    var $hamburger = $('.hamburger');
    var $navigation = $('.header-nav');

    $(document).on('click', '.hamburger', function(e) {
        e.preventDefault();
        $(this).toggleClass('open');

        if ($hamburger.hasClass('open')) {
            $navigation.addClass('open');
        } else {
            $navigation.removeClass('open');
        }
    });
});
