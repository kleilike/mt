'use strict';

const configFTP = {
  host: 'mt.kleilike.ru',
  user: 'mt@kleilike.ru',
  password: '~q3_AN=}^a3;',
  parallel: 2,
  port: 21
};

const gulp = require('gulp');
const del = require('del');
const newer = require('gulp-newer');
const debug = require('gulp-debug');
const rename = require('gulp-rename');
const remember = require('gulp-remember');
// const sourcemaps = require('gulp-sourcemaps');

const sass = require('gulp-sass');
const postCSS = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const assets = require('postcss-assets');
const inlineSVG = require('postcss-inline-svg');
// const cssnano = require('cssnano');
const lost = require('lost');

const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

const browserSync = require('browser-sync').create();

const ftp = require('vinyl-ftp');
const zip = require('gulp-zip');

const path = {
  src: {
    html: 'src/**/*.html',
    styles: 'src/scss/style.scss',
    images: 'src/img/**/*.*',
    scripts: ['src/js/*.js', 'src/js/pages/*.js'],
    static: {
      favicon: [
        'src/favicon.ico',
        'src/fluidicon.png',
        'src/apple-touch-icon.png',
        'src/apple-touch-icon-57x57.png',
        'src/apple-touch-icon-60x60.png',
        'src/apple-touch-icon-72x72.png',
        'src/apple-touch-icon-76x76.png',
        'src/apple-touch-icon-114x114.png',
        'src/apple-touch-icon-120x120.png',
        'src/apple-touch-icon-144x144.png',
        'src/apple-touch-icon-152x152.png',
        'src/apple-touch-icon-180x180.png',
        'src/pinned-octocat.svg'
      ],
      vendor: 'src/vendor/**/*',
      font: 'src/font/**/*'
    }
  },
  dest: {
    html: 'dest',
    styles: 'dest/css',
    images: 'dest/img',
    scripts: 'dest/js',
    static: {
      favicon: 'dest',
      vendor: 'dest/vendor',
      font: 'dest/font'
    }
  },
  watch: {
    html: 'src/**/*.html',
    styles: 'src/scss/**/*.scss',
    images: 'src/img/**/*.*',
    scripts: 'src/js/**/*.js',
    static: {
      favicon: [
        'src/favicon.ico',
        'src/fluidicon.png',
        'src/apple-touch-icon.png',
        'src/apple-touch-icon-57x57.png',
        'src/apple-touch-icon-60x60.png',
        'src/apple-touch-icon-72x72.png',
        'src/apple-touch-icon-76x76.png',
        'src/apple-touch-icon-114x114.png',
        'src/apple-touch-icon-120x120.png',
        'src/apple-touch-icon-144x144.png',
        'src/apple-touch-icon-152x152.png',
        'src/apple-touch-icon-180x180.png',
        'src/pinned-octocat.svg',
      ],
      vendor: 'src/vendor/**/*',
      font: 'src/font/**/*'
    }
  }
};

gulp.task('clean', function () {
  return del(['dest', 'site.zip']);
});

gulp.task('html', function () {
  return gulp.src(path.src.html, {since: gulp.lastRun('html')})
    .pipe(newer(path.dest.html))
    .pipe(debug({'title':'html'}))
    .pipe(gulp.dest(path.dest.html));
});

gulp.task('style', function () {
  let config = [
    assets(),
    inlineSVG(),
    autoprefixer({browsers: ['last 5 versions', 'ie 6-8', 'Firefox >= 3.5', 'iOS >= 4', 'Android >= 2.3']}),
    lost(),
    // cssnano()
  ];

  return gulp.src(path.src.styles)
    // .pipe(sourcemaps.init())
    .pipe(remember('style'))
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(postCSS(config))
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.dest.styles));
});

gulp.task('image', function () {
  return gulp.src(path.src.images, {since: gulp.lastRun('image')})
    .pipe(newer(path.dest.images))
    .pipe(imagemin({
      interlaced: true,
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(path.dest.images));
});

gulp.task('script', function () {
  return gulp.src(path.src.scripts, {since: gulp.lastRun('script')})
    .pipe(remember('script'))
    // .pipe(sourcemaps.init())
    // .pipe(uglify())
    .pipe(concat('script.js'))
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.dest.scripts));
});

gulp.task('static:favicon', function () {
  return gulp.src(path.src.static.favicon, {since: gulp.lastRun('static:favicon')})
    .pipe(remember('static:favicon'))
    .pipe(gulp.dest(path.dest.static.favicon));
});

gulp.task('static:vendor', function () {
  return gulp.src(path.src.static.vendor, {since: gulp.lastRun('static:vendor')})
    .pipe(remember('static:vendor'))
    .pipe(gulp.dest(path.dest.static.vendor));
});

gulp.task('static:font', function () {
  return gulp.src(path.src.static.font, {since: gulp.lastRun('static:font')})
    .pipe(remember('static:font'))
    .pipe(gulp.dest(path.dest.static.font));
});

gulp.task('static', gulp.series('static:favicon', 'static:vendor', 'static:font'));

gulp.task('server', function () {
  browserSync.init({
    server: {
      baseDir: 'dest'
    }
  });

  gulp.watch(path.watch.html).on('change', gulp.series('html', browserSync.reload));
  gulp.watch(path.watch.styles).on('change', gulp.series('style', browserSync.reload));
  gulp.watch(path.watch.images).on('add', gulp.series('image', browserSync.reload));
  gulp.watch(path.watch.images).on('change', gulp.series('image', browserSync.reload));
  gulp.watch(path.watch.scripts).on('change', gulp.series('script', browserSync.reload));
  gulp.watch(path.watch.static.favicon).on('change', gulp.series('static:favicon', browserSync.reload));
  gulp.watch(path.watch.static.vendor).on('change', gulp.series('static:vendor', browserSync.reload));
  gulp.watch(path.watch.static.font).on('change', gulp.series('static:font', browserSync.reload));
});

gulp.task('zip', function () {
  return gulp.src('./dest/**/*.*')
    .pipe(zip('site.zip'))
    .pipe(gulp.dest('./'));
});

gulp.task('ftp', function () {
  let config = ftp.create(configFTP);
  let files = [
    './dest/**/*.*'
  ];

  return gulp.src(files, {base: './dest', buffer: false})
    .pipe(config.dest(''));
});

// gulp.task('upload', gulp.series('clean', 'build', 'ftp'));
gulp.task('build', gulp.series('html', 'style', 'image', 'script', 'static'));
gulp.task('default', gulp.series('build', 'server'));
